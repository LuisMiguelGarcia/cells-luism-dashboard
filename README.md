# &lt;cells-luism-dashboard&gt;

Your component description.

![Polymer 2.x](https://img.shields.io/badge/Polymer-2.x-green.svg)
Example:
```html
<cells-luism-dashboard></cells-luism-dashboard>
```

## Styling
  The following custom properties and mixins are available for styling:

  ### Custom Properties
  | Custom Property     | Selector | CSS Property | Value       |
  | ------------------- | -------- | ------------ | ----------- |
  | --cells-fontDefault | :host    | font-family  |  sans-serif |
  ### @apply
  | Mixins    | Selector | Value |
  | --------- | -------- | ----- |
  | --cells-luism-dashboard | :host    | {} |
