{
  const {
    html,
  } = Polymer;
  /**
    `<cells-luism-dashboard>` Description.

    Example:

    ```html
    <cells-luism-dashboard></cells-luism-dashboard>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-luism-dashboard | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsLuismDashboard extends Polymer.Element {

    static get is() {
      return 'cells-luism-dashboard';
    }

    static get template() {
      return html `
      <style include="cells-luism-dashboard-styles cells-luism-dashboard-shared-styles"></style>
      <slot></slot>
      
      <input type="text" id="txt_text" value="{{filtertxt1::input}}" placeholder="Buscar...">
      
      <iron-ajax id="cardUno" 
          auto 
          url="https://api.chucknorris.io/jokes/random" 
          handle-as="json" 
          last-response ="{{responseChucknorris}}">
      </iron-ajax>

      <div class="card" style="margin-bottom: 10px;">

          <cells-mocks-component alumnos="{{alumnos}}"></cells-mocks-component>

          <template is="dom-repeat" items="{{alumnos}}" filter="{{filtrado(filtertxt1)}}">

            <STRONG><h1>My Perfil</h1></STRONG>

            <img src="{{item.img}}" height="150px" width="250px" class="img" >

            <strong><p>First Name:</p></strong>
            <p>{{item.name}}</p>

            <strong>Last Name:</strong>
            <p>{{item.last}}</p>

            <strong><p>Address:</p></strong>
            <p>{{item.address}}</p>

            <strong><p>Hobbies:</p></strong>
            <p>{{item.hobbies}}</p>

          </template>

        </div>
      `;
    }
    static get properties() {
      return {
        pro: {
          type: Boolean,
          value: false
        },

        responseChucknorris: {
          type: Object
        },

        alumnos: {
          type: Object,
          value: {},
          notify: true
        }
      };
    }

    ready() {
      super.ready();
      console.log(this.responseChucknorris);
    }

    viwData(e) {
      console.log(e.detail.value);
      if (e.detail.value === true) {
        this.set('pro', e.detail.value);
      }
    }

    filtrado(string) {
      if (!string) {
        return null;
      } else {
        string = string.toLowerCase();
        return function(alumnos) {
          var first = alumnos.name.toLowerCase();
          var last = alumnos.last.toLowerCase();
          return (first.indexOf(string) !== -1 || last.indexOf(string) !== -1);
        };
      }
    }
  }
  customElements.define(CellsLuismDashboard.is, CellsLuismDashboard);
}